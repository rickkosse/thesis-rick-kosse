# Proces_data.py
# Rick Kosse s3243508
# date: 11-12-2017
# The program processes data for the machine learning classifer.

import json
import re
import nltk
from os import listdir
import os
import operator
from functools import partial
import bs4
from random import shuffle
from itertools import groupby, chain, islice


def central_tokenize(string):
    """" removes all uneccessary words, players names, cities etc. """
    words = []
    clean = ""
    regexp = re.compile(r"([a-zA-Z])\1\1\1*")
    stripped=""
    for word in string:
        if "#" in word or "@" in word or "fc twente" in word or "twente" in word or "enschede" in word or "psv" in word or "bosz" in word or "klaasjan" in word or "toornstra" in word or "cocu" in word or "kongolo" in word or "heijden" in word or "moreno" in word or "pröpper" in word or "jong" in word or "propper" in word or "karim" in word or "berghuis" in word or "tonny" in word  or "sven" in word or "schone" in word or "karim" in word or "ziyech" in word or "beek" in word or "kuyt" in word or "veltman" in word or "bilal" in word or "jones" in word or "sanchez" in word or "huntelaar" in word or "frenkie" in word or "dirk" in word or "vilhena" in word or "davinson" in word  or "dijks" in word or "nicolai" in word or "kasper" in word or "lozano" in word or "eindhoven" in word or "ajax" in word or "amsterdam" in word or "feyenoord" in word or "rotterdam" in word or "dolberg" in word or "marsman" in word or "diks" in word or "jürgen" in word or "viergever" in word or "neres" in word or "hake" in word or "kramer" in word  or "younes" in word  or "steven" in word  or "hakim" in word or "keizer" in word or "nelom" in word or ";'\"[:]_)(.!?=*-><'" in word \
        or "0123456789" in word or (is_psv_player(word)) == True or (is_ajax_player(word)) == True or (is_feye_player(word)) == True or (is_twente_player(word)) == True:
            word = ""
            words.append(word)
        else:
            words.append(word)
    
    string = " ".join(c for c in words)
    for word in string.split():
        match = re.search(regexp, word)
        if match:
            word = ''.join(chain.from_iterable(islice(g, 2) for k, g in groupby(word)))
            stripped = stripped + word + " "

        else:
            stripped = stripped + word + " "
    stripped = stripped.rstrip()
    out = "".join(c for c in stripped if c not in ('!','.',':','#','?','/','$','@', '&', '*', '(', ')', '=', '-', '+', ',', '%', '`', '\\','…', '‘','”', '\'','^','0123456789','0','1','2','3','4','5','6','7','8','9'))

    return out

def is_psv_player(name):
        """" removes all uneccessary players names form PSV """
    with open("psv_selectie.txt", "r") as infile:    
        
        players = [players.lower().rstrip() for players in infile]

    if name.lower() in players:
        return True
    else:
        return False

def is_ajax_player(name):
     """" removes all uneccessary players names form Ajax """
    with open("ajax_selectie.txt", "r") as infile:    
        
        players = [players.lower().rstrip() for players in infile]

    if name.lower() in players:
        return True
    else:
        return False

def is_feye_player(name):
     """" removes all uneccessary players names form Feyenoord """
    with open("feyenoord_selectie.txt", "r") as infile:    
        
        players = [players.lower().rstrip() for players in infile]

    if name.lower() in players:
        return True
    else:
        return False

def is_twente_player(name):
     """" removes all uneccessary players names form Twente """
    with open("twente_selectie.txt", "r") as infile:    
        
        players = [players.lower().rstrip() for players in infile]

    if name.lower() in players:
        return True
    else:
        return False

def find_files(directory):
     """" Look for the data """
    paths = []
    for root, directories, files in os.walk(directory):
        for filename in files:
            filepath = os.path.join(root, filename)
            paths.append(filepath)
    return [path for path in paths if path.endswith("tweets.out") or path.endswith("ajax.tweets")]


def main():
    amount_users = 0
    user_account50 = []
    user_tweets50 = []
    user_account300 = []
    path = "/Users/rickkosse/Documents/RUG/scriptie/twitter API/more/"
    tweets_50 = 0
    tweets_300 = 0
    for fl in find_files("total/psv"):
        tweets_50 = 0
        tweets_300 = 0
        user_tweets300 =[]
        user_tweets50 = []
        alles=[]
        print(fl)
        #open file
        user_file= open(fl).readlines()
        # count users
        amount_users += 1
        for line in user_file:
            line = line.rstrip()
            words = line.split()
            #proces
            new_line = central_tokenize(words)
            if tweets_50 < 50:
                #count 50 tweets
                user_tweets50.append(new_line)
                tweets_50 += 1

            if tweets_300 < 500:
                #count 300 tweets
                user_tweets300.append(new_line) 
                tweets_300 += 1

        all_50 = "".join(user_tweets50)
        all_300 = "".join(user_tweets300)
        user_account300.append(all_300)
        user_account50.append(all_50)
    print("aantal user is:", amount_users)
    print("voor de 50 tweets", len(user_account50)/ amount_users)
    print("voor de 500 tweets", len(user_account300)/ amount_users)
    
    with open(path+"psv._50.out", "w") as outfile:  
        outfile.write('\n'.join(str(s) for s in user_account50)) 
    outfile.close()

    with open(path+"psv.all_training.out", "w") as outfile:  
        outfile.write('\n'.join(str(s) for s in user_account300)) 
    outfile.close()

    amount_users = 0
    user_account50 = []
    user_tweets50 = []
    user_account300 = []
    tweets_50 = 0
    tweets_300 = 0
    for fl in find_files("total/ajax"):
        tweets_50 = 0
        tweets_300 = 0
        user_tweets300 =[]
        user_tweets50 = []
        alles=[]
        print(fl)
        user_file= open(fl).readlines()
        amount_users += 1
        for line in user_file:
            line = line.rstrip()
            words = line.split()
            new_line = central_tokenize(words)
            if tweets_50 < 50:
                user_tweets50.append(new_line)
                tweets_50 += 1

            if tweets_300 < 500:
                user_tweets300.append(new_line) 
                tweets_300 += 1

        all_50 = "".join(user_tweets50)
        all_300 = "".join(user_tweets300)
        user_account300.append(all_300)
        user_account50.append(all_50)
    print("aantal user is:", amount_users)
    print("voor de 50 tweets", len(user_account50)/ amount_users)
    print("voor de 500 tweets", len(user_account300)/ amount_users)
    
    with open(path+"ajax._50.out", "w") as outfile:  
        outfile.write('\n'.join(str(s) for s in user_account50))
    outfile.close()
    with open(path+"ajax.all_training.out", "w") as outfile:  
        outfile.write('\n'.join(str(s) for s in user_account300)) 
    outfile.close()

    amount_users = 0
    user_account50 = []
    user_tweets50 = []
    user_account300 = []
    tweets_50 = 0
    tweets_300 = 0
    for fl in find_files("total/feye"):
        tweets_50 = 0
        tweets_300 = 0
        user_tweets300 =[]
        user_tweets50 = []
        alles=[]
        print(fl)
        user_file= open(fl).readlines()
        amount_users += 1
        for line in user_file:
            line = line.rstrip()
            words = line.split()
            new_line = central_tokenize(words)
            if tweets_50 < 50:
                user_tweets50.append(new_line)
                tweets_50 += 1

            if tweets_300 < 500:
                user_tweets300.append(new_line) 
                tweets_300 += 1


        all_50 = "".join(user_tweets50)
        all_300 = "".join(user_tweets300)
        user_account300.append(all_300)
        user_account50.append(all_50)
    print("aantal user is:", amount_users)
    print("voor de 50 tweets", len(user_account50)/ amount_users)
    print("voor de 500 tweets", len(user_account300)/ amount_users)
    with open(path+"feyenoord._50.out", "w") as outfile:  
        outfile.write('\n'.join(str(s) for s in user_account50)) 
    outfile.close()

    with open(path+"feyenoord.all_training.out", "w") as outfile:  
        outfile.write('\n'.join(str(s) for s in user_account300)) 
    outfile.close()

    amount_users = 0
    user_account50 = []
    user_tweets50 = []
    user_account300 = []
    tweets_50 = 0
    tweets_300 = 0
    for fl in find_files("total/twente"):
        tweets_50 = 0
        tweets_300 = 0
        user_tweets300 =[]
        user_tweets50 = []
        alles=[]
        print(fl)
        user_file= open(fl).readlines()
        amount_users += 1
        for line in user_file:
            line = line.rstrip()
            words = line.split()
            new_line = central_tokenize(words)
            if tweets_50 < 50:
                user_tweets50.append(new_line)
                tweets_50 += 1

            if tweets_300 < 500:
                user_tweets300.append(new_line) 
                tweets_300 += 1


        all_50 = "".join(user_tweets50)
        all_300 = "".join(user_tweets300)
        user_account300.append(all_300)
        user_account50.append(all_50)
    print("aantal user is:", amount_users)
    print("voor de 50 tweets", len(user_account50)/ amount_users)
    print("voor de 500 tweets", len(user_account300)/ amount_users)
    
    with open(path+"twente._50.out", "w") as outfile:  
        outfile.write('\n'.join(str(s) for s in user_account50)) 
    outfile.close()

    with open(path+"twente.all_training.out", "w") as outfile:  
        outfile.write('\n'.join(str(s) for s in user_account300)) 
    outfile.close()


if __name__ == '__main__':
    main()

