# crossvalidation.py
# Rick Kosse s3243508
# date: 11-12-2017
# This program takes four files as input, splits it with respect to the distribution and does a crossvaldiation method on the data. 

import nltk
from nltk.corpus import stopwords
from collections import defaultdict 
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
import numpy as np
import random
import sys
from sklearn.pipeline import FeatureUnion
import nltk
from nltk.corpus import stopwords
from collections import defaultdict 
from sklearn.base import BaseEstimator,TransformerMixin
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score
from sklearn.naive_bayes import GaussianNB, MultinomialNB, BernoulliNB
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn import preprocessing
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import classification_report
import pandas as pd
from sklearn.model_selection import StratifiedKFold

class Featurizer(TransformerMixin):
    """Our own featurizer: extract features from each document for DictVectorizer"""

    PREFIX_WORD_NGRAM="W:"
    
    def fit(self, x, y=None):
        return self
    
    def transform(self, X):
        """
        here we could add more features!
        """
        out= [self._word_ngrams(text,ngram=self.word_ngrams)
                for text in X]
        return out

    def __init__(self,word_ngrams="1-3",binary=True,lowercase=False,remove_stopwords=True):
        """
        binary: whether to use 1/0 values or counts
        lowercase: convert text to lowercase
        remove_stopwords: True/False
        """
        self.DELIM=" "
        self.data = [] # will hold data (list of dictionaries, one for every instance)
        self.lowercase=lowercase
        self.binary=binary
        self.remove_stopwords = remove_stopwords
        self.stopwords = stopwords.words('dutch')
        self.word_ngrams=word_ngrams

    def _word_ngrams(self,text,ngram="1-2-3"):

        d={} #dictionary that holds features for current instance
        if self.lowercase:
            text = text.lower()

        words=text.split(self.DELIM)
        if self.remove_stopwords:
            words = [w for w in words if w not in self.stopwords]

        for n in ngram.split("-"):
            for gram in nltk.ngrams(words, int(n)):
                gram = self.PREFIX_WORD_NGRAM + "_".join(gram)
                if self.binary:
                     d[gram] = 1 #binary
                else:
                    d[gram] += 1
        return d

        

    
if __name__ == "__main__":
    import doctest
    doctest.testmod()


def load_sentiment_sentences_and_labels():
    """
    loads the movie review data
    """
    le = preprocessing.LabelEncoder()
    psv_sentences = open("/Users/rickkosse/Documents/RUG/scriptie/twitter API/more/psv._50.out").readlines()
    twente_sentences = open("/Users/rickkosse/Documents/RUG/scriptie/twitter API/more/twente._50.out").readlines()
    feye_sentences = open("/Users/rickkosse/Documents/RUG/scriptie/twitter API/more/feyenoord._50.out").readlines()
    ajax_sentences = open("/Users/rickkosse/Documents/RUG/scriptie/twitter API/more/ajax._50.out").readlines()

    ajax_labels = [0 for ajax in ajax_sentences]
    psv_labels = [1 for psv in psv_sentences]
    feye_labels = [2 for feyenoord in feye_sentences]
    twente_labels = [3 for twente in twente_sentences]


    sentences = np.concatenate([feye_sentences,ajax_sentences, psv_sentences, twente_sentences])
    labels = np.concatenate([feye_labels,ajax_labels, psv_labels, twente_labels],axis=0)

    assert(len(sentences)==len(labels))
    data = list(zip(sentences,labels))
    random.shuffle(data)

    print("split data..", file=sys.stderr)
    split_point = int(0.80*len(data))
    
    sentences = [sentence for sentence, label in data]
    labels = [label for sentence, label in data]

    print("kfold")
    X = np.array(sentences)
    y = np.array(labels)
    skf = StratifiedKFold(n_splits=10, shuffle= True)
    skf.get_n_splits(X, y)
    for train_index, test_index in skf.split(X, y):
       print("TRAIN:", train_index, "TEST:", test_index)
       X_train, X_test = X[train_index], X[test_index]
       y_train, y_test = y[train_index], y[test_index]

    
    assert(len(X_train)==len(y_train))
    assert(len(X_test)==len(y_test))

    return X_train, y_train, X_test, y_test

random.seed(113)
X_train, y_train, X_test, y_test = load_sentiment_sentences_and_labels()

print("vectorize data..", file=sys.stderr)
featurizer = Featurizer(word_ngrams="1-2-3")
vectorizer = DictVectorizer()


# first extract the features (as dictionaries)
X_train_dict = featurizer.fit_transform(X_train)
X_test_dict = featurizer.transform(X_test)

# then convert them to the internal representation (maps each feature to an id)
X_train = vectorizer.fit_transform(X_train_dict)
X_test = vectorizer.transform(X_test_dict)

classifier = MultinomialNB()

print("cross_validation...")
clf = classifier
scores = cross_val_score(clf, X_train, y_train, cv=10, scoring='f1_macro')
print("F1-score",scores)
print("F1-score mean: %0.4f (+/- %0.4f)" % (scores.mean(), scores.std() * 2))

# print("train model..", file=sys.stderr)
# classifier.fit(X_train, y_train)
# ##
# print("predict..", file=sys.stderr)
# y_predicted = classifier.predict(X_test)
###
# print("Accuracy:", accuracy_score(y_test, y_predicted), file=sys.stderr)
# print("F1-score:", f1_score(y_test, y_predicted, average='weighted'))


def show_most_informative_features(vectorizer, clf, n=20):
    feature_names = vectorizer.get_feature_names()
    for i in range(0,len(clf.coef_)):
        coefs_with_fns = sorted(zip(clf.coef_[i], feature_names))
        top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])
        print("I",i)
        for (coef_1, fn_1), (coef_2, fn_2) in top:
            print("\t%.4f\t%-15s\t\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2))


show_most_informative_features(vectorizer, classifier, n=20)