# twitter_stream.py
# Rick Kosse s3243508
# date: 11-12-2017
# This program retrieves 800 tweets from the users

import twitter
import json
from twitter import Twitter, OAuth, TwitterStream
import csv
import json
import re
import nltk
from os import listdir
import os
import operator
from functools import partial
import bs4
import tweepy
import langid

def remove_urls(text):
    """This method takes a string as argument and returns string without urls"""
    with_out = []
    regex = re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', re.DOTALL)
    urls = re.findall(regex, text)
    for url in urls:
        text = text.replace(url[0],'')
        with_out.append(text)
    return with_out

def find_files(directory):
    paths = []
    for root, directories, files in os.walk(directory):
        for filename in files:
            filepath = os.path.join(root, filename)
            paths.append(filepath)
    return [path for path in paths if path.endswith(".out")]

def main():
    token = []
    aja_path = "/Users/rickkosse/Documents/RUG/scriptie/twitter API/total/ajax/"
    feye_path = "/Users/rickkosse/Documents/RUG/scriptie/twitter API/total/feye/"
    psv_path = "/Users/rickkosse/Documents/RUG/scriptie/twitter API/total/psv/"
    twente_path = "/Users/rickkosse/Documents/RUG/scriptie/twitter API/total/twente/"
    for fl in find_files("total"):
        with open(fl, "r") as infile:
            print(fl)
            if fl == "total/labeld_users.ajax.out": 
                for line in infile:
                    line = line.rstrip().split(",")
                    user_id = line[1]
                    screen_name = line[0]
                    tweets = get_tweets(user_id, screen_name)
                    if tweets is not (None):
                        token = []
                        for tweet in tweets:
                            with_out= remove_urls(tweet)
                            alltweets = " ".join(str(s) for s in with_out)
                            token.append(alltweets)
                        if len(token) >= 300:
                            with open(aja_path+screen_name +".ajax.tweets", "w") as outfile:  
                                new_list = token[:300]
                                outfile.write('\n'.join(str(s) for s in new_list if not s == "")) 
                            outfile.close()

            if fl == "total/labeld_users.psv.out": 
                for line in infile:
                    line = line.rstrip().split(",")
                    user_id = line[1]
                    screen_name = line[0]
                    tweets = get_tweets(user_id, screen_name)
                    if tweets is not (None):
                        token = []
                        for tweet in tweets:
                            with_out= remove_urls(tweet)
                            alltweets = " ".join(str(s) for s in with_out)
                            token.append(alltweets)
                        if len(token) >= 300:
                            with open(psv_path+screen_name +".psv.tweets.out", "w") as outfile:  
                                new_list = token[:300]
                                outfile.write('\n'.join(str(s) for s in new_list if not s == "")) 
                            outfile.close()


            if fl == "total/labeld_users.twente.out": 
                for line in infile:
                    line = line.rstrip().split(",")
                    user_id = line[1]
                    screen_name = line[0]
                    tweets = get_tweets(user_id, screen_name)
                    if tweets is not (None):
                        token = []
                        for tweet in tweets:
                            print(tweet)
                            with_out= remove_urls(tweet)
                            alltweets = " ".join(str(s) for s in with_out)
                            token.append(alltweets)
                        if len(token) >= 300:
                            with open(twente_path+screen_name +".twente.tweets.out", "w") as outfile:  
                                new_list = token[:300]
                                outfile.write('\n'.join(str(s) for s in new_list if not s == "")) 
                            outfile.close()

            elif fl == "total/labeld_users.feye.out": 
                for line in infile:
                    line = line.rstrip().split(",")
                    user_id = line[1]
                    screen_name = line[0]
                    tweets = get_tweets(user_id, screen_name)
                    if tweets is not (None):
                        token = []
                        for tweet in tweets:
                            with_out= remove_urls(tweet)
                            alltweets = " ".join(str(s) for s in with_out)
                            token.append(alltweets)
                        if len(token) >= 300:
                            with open(feye_path+screen_name +".feye.tweets.out", "w") as outfile:  
                                new_list = token[:300]
                                outfile.write('\n'.join(str(s) for s in new_list if not s == "")) 
                            outfile.close()


def get_tweets(user_id, screen_name):
    """ Handles the requests"""
    ckey="fWNMjzARyW5tCrG2wLdDs3qU8"
    csecret="QbSDKgj910vOPqb0MFboqGaqTdoBqMyVIZouOfBdue7BaTfrH9"
    atoken="852138461750493184-mA7yLXmu6QmKx61xjs5q8A4JCu80eVR"
    asecret="x4lci7aJwTPkefSzPJihTKwFfvWub2Qk2JRiauFsu7Ozb"
    amount_tweets= 0
    com_tweet =[]
    alltweets = []

    # authenticate twitter account
    auth = tweepy.OAuthHandler(ckey, csecret)
    auth.set_access_token(atoken, asecret)

    api = tweepy.API(auth)

    try:
        new_tweets = api.user_timeline(user_id= user_id, count=200)
        alltweets.extend(new_tweets)

        try:
        #save the id of the oldest tweet less one
            oldest = alltweets[-1].id - 1

        #keep grabbing tweets until there are no tweets left to grab
            while len(alltweets) < 800:
                    print ("getting tweets before", oldest)
                    print(screen_name)
                    
                    #all subsiquent requests use the max_id param to prevent duplicates
                    new_tweets = api.user_timeline(user_id= user_id, count=200,max_id=oldest)
                    
                    #save most recent tweets
                    alltweets.extend(new_tweets)

                    #update the id of the oldest tweet less one
                    x = oldest 
                    oldest = alltweets[-1].id - 1
                    y = oldest

                    # if it reached the last tweet, the id doesn't update so break.
                    if x == y:
                        break
                    
                    print ("...", len(alltweets), "tweets downloaded so far")
        except IndexError as b:
            print(b)
            print("Doesn't have any tweets...")
            pass
        for tweet in alltweets:
            #check if the tweet is not protected
            if (not tweet.user.protected):
                #check if the tweet is not retweeted
                if (not tweet.retweeted) and ('RT @' not in tweet.text):
                    #check if the tweet is in Dutch
                    if langid.classify(tweet.text)[0] == "nl":
                        com_tweet.append(tweet.text)
        return com_tweet
    except tweepy.error.TweepError as e:
        print(e)
        print("Failed to run the command on that user, Skipping...")
        pass
  
if __name__ == '__main__':
    main()

