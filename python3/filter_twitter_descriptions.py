#filter_twitter_descriptions.py
# Rick Kosse s3243508
# date: 11-12-2017
# This program loops through twitterdata and filters out the description and corresponding user. Then, it sorts the user per club if one of the clubs is mentioned in the description
# If so it writes the user to an outfile.

import json
from os import listdir
import re
import nltk
from os import listdir
import os
import operator
from nltk.corpus import wordnet
from nltk.tag import StanfordNERTagger
from nltk.stem.wordnet import WordNetLemmatizer
import wikipedia
from functools import partial
import bs4



def tokenize(string):
    """This method lowercases a string and removes punctuation, returns list of words"""
    stripped = ""
    tweet = []
    for i in string.lower():
        if i.isalnum() or i == '#' or i == '@' or i == 'II':
            stripped += i
        else:
            stripped += " "
    nopunct_lower = stripped.split()
    for i in nopunct_lower:
        if i.startswith('#') or i.startswith('@') or i.startswith('rt'):
            continue
        else:
            tweet.append(i)
    return tweet


def extract_tweets(infile):
    """takes file, reads file, returns list of tuples with text of tweet and username"""
    tweets = []
    for line in open(infile, 'r'):
        if not ('retweeted_status' in line):
            tweets.append((json.loads(line)['text'], json.loads(line)['user']['screen_name'],json.loads(line)['user']['description'],json.loads(line)['user']['id']))
    
    return tweets



def find_files(directory):
    paths = []
    for root, directories, files in os.walk(directory):
        for filename in files:
            filepath = os.path.join(root, filename)
            paths.append(filepath)
    return [path for path in paths if path.endswith(".out")]


def main():
    ajax_des=[]
    fcg_des=[]
    feye_des=[]
    vitesse_des=[]
    gae_des=[]
    az_des=[]
    heere_des=[]
    willem_des=[]
    ado_des=[]
    pec_des=[]
    twente_des=[]
    sparta_des=[]
    excel_des=[]
    nec_des=[]
    roda_des=[]
    psv_des=[]
    utrecht_des=[]
    hera_des=[]
    
# looks into files and sorts out the tweets, description and user. 
    for fl in find_files("tweets"):
        with open(fl, "r") as infile:
            print(fl)
            for tweet, user, description, id in extract_tweets(fl):
                #there should be a description
                if description is not None:
                    #the description should not contain the word news.
                    if "nieuws" not in description:
                        tokenized = tokenize(description)
                        new_description = " ".join(tokenized)
                        # sort the description per club
                        if "ajax" in new_description:
                            useful_tweet = (user, str(id))
                            ajax_des.append(useful_tweet)
                        
                        if "az alkmaar" in new_description:
                            useful_tweet = (user, str(id))
                            az_des.append(useful_tweet)

                        elif "feyenoord" in new_description:
                            useful_tweet = (user, str(id))
                            feye_des.append(useful_tweet)

                        elif "psv" in new_description:
                            useful_tweet = (user, str(id))
                            psv_des.append(useful_tweet)

                        elif "fc twente" in new_description:
                            useful_tweet = (user, str(id))
                            twente_des.append(useful_tweet)

                        elif "sc heerenveen" in new_description:
                            useful_tweet = (user, str(id))
                            heere_des.append(useful_tweet)

                        elif "fc utrecht" in new_description:
                            useful_tweet = (user, str(id))
                            utrecht_des.append(useful_tweet)

                        elif "heracles almelo" in new_description:
                            useful_tweet = (user, str(id))
                            hera_des.append(useful_tweet)

                        elif "ado den haag" in new_description:
                            useful_tweet = (user, str(id))
                            ado_des.append(useful_tweet)
     
                        elif "fc groningen"  in new_description:
                            useful_tweet = (user, str(id))
                            fcg_des.append(useful_tweet)
            
                        elif "go ahead eagles" in new_description:
                            useful_tweet = (user, str(id))
                            gae_des.append(useful_tweet)

                        elif "vitesse"in new_description:
                            useful_tweet = (user, str(id))
                            vitesse_des.append(useful_tweet)

                        elif "excelsior"in new_description:
                            useful_tweet = (user, str(id))
                            excel_des.append(useful_tweet)

                        elif "willem II "in new_description:
                            useful_tweet = (user, str(id))
                            willem_des.append(useful_tweet)

                        elif "pec zwolle "in new_description:
                            useful_tweet = (user, str(id))
                            pec_des.append(useful_tweet)

                        elif "roda jc"in new_description:
                            useful_tweet = (user, str(id))
                            roda_des.append(useful_tweet)

                        elif "nec nijmegen"in new_description:
                            useful_tweet = (user, str(id))
                            nec_des.append(useful_tweet)

                        elif "sparta rotterdam"in new_description:
                            useful_tweet = (user, str(id))
                            sparta_des.append(useful_tweet)
                        

# remove dulpicates
    ajax_dup = set(ajax_des)
    vitesse_dup = set(vitesse_des)
    gae_dup = set(gae_des)
    fcg_dup = set(fcg_des)
    feye_dup = set(feye_des)
    az_dup = set(az_des)
    sparta_dup = set(sparta_des)
    nec_dup = set(nec_des)
    roda_dup = set(roda_des)
    pec_dup = set(pec_des)
    willem_dup = set(willem_des)
    hera_dup = set(hera_des)
    exel_dup = set(excel_des)
    utrecht_dup = set(utrecht_des)
    twente_dup = set(twente_des)
    ado_dup = set(ado_des)
    psv_dup = set(psv_des)
    heere_dup = set(heere_des)

#write to the corresponding file of a club
    with open("labeld_users.ajax", "w") as ajax_outfile:
        for item in ajax_dup:
            ajax_outfile.write(', '.join(str(s) for s in item) + '\n')
    ajax_outfile.close()
    with open("labeld_users.feye", "w") as feye_outfile:
        for item in feye_dup:
            feye_outfile.write(', '.join(str(s) for s in item) + '\n')
    feye_outfile.close() 
    with open("labeld_users.fcg", "w") as fcg_outfile:
        for item in fcg_dup:
            fcg_outfile.write(', '.join(str(s) for s in item) + '\n')
    fcg_outfile.close()
    with open("labeld_users.vitesse", "w") as vitesse_outfile:
        for item in vitesse_dup:
            vitesse_outfile.write(', '.join(str(s) for s in item) + '\n')
    vitesse_outfile.close() 
    with open("labeld_users.gae", "w") as gae_outfile:
        for item in gae_dup:
            gae_outfile.write(', '.join(str(s) for s in item) + '\n')
    gae_outfile.close()
    with open("labeld_users.az", "w") as az_outfile:
        for item in az_dup:
            az_outfile.write(', '.join(str(s) for s in item) + '\n')
    az_outfile.close()
    with open("labeld_users.utrecht", "w") as utrecht_outfile:
        for item in utrecht_dup:
            utrecht_outfile.write(', '.join(str(s) for s in item) + '\n')
    utrecht_outfile.close() 
    with open("labeld_users.twente", "w") as twente_outfile:
        for item in twente_dup:
            twente_outfile.write(', '.join(str(s) for s in item) + '\n')
    twente_outfile.close()
    with open("labeld_users.excel", "w") as excel_outfile:
        for item in exel_dup:
            excel_outfile.write(', '.join(str(s) for s in item) + '\n')
    excel_outfile.close() 
    with open("labeld_users.pec", "w") as pec_outfile:
        for item in pec_dup:
            pec_outfile.write(', '.join(str(s) for s in item) + '\n')
    pec_outfile.close()
    with open("labeld_users.nec", "w") as nec_outfile:
        for item in nec_dup:
            nec_outfile.write(', '.join(str(s) for s in item) + '\n')
    nec_outfile.close()
    with open("labeld_users.roda", "w") as roda_outfile:
        for item in roda_dup:
            roda_outfile.write(', '.join(str(s) for s in item) + '\n')
    roda_outfile.close() 
    with open("labeld_users.ado", "w") as ado_outfile:
        for item in ado_dup:
            ado_outfile.write(', '.join(str(s) for s in item) + '\n')
    ado_outfile.close()
    with open("labeld_users.willem", "w") as willem_outfile:
        for item in willem_dup:
            willem_outfile.write(', '.join(str(s) for s in item) + '\n')
    willem_outfile.close() 
    with open("labeld_users.hera", "w") as hera_outfile:
        for item in hera_dup:
            hera_outfile.write(', '.join(str(s) for s in item) + '\n')
    hera_outfile.close()
    with open("labeld_users.sparta", "w") as sparta_outfile:
        for item in sparta_dup:
            sparta_outfile.write(', '.join(str(s) for s in item) + '\n')
    sparta_outfile.close()
    with open("labeld_users.psv", "w") as psv_outfile:
        for item in psv_dup:
            psv_outfile.write(', '.join(str(s) for s in item) + '\n')
    psv_outfile.close() 
    with open("labeld_users.heere", "w") as heere_outfile:
        for item in heere_dup:
            heere_outfile.write(', '.join(str(s) for s in item) + '\n')
    heere_outfile.close()                          

   
if __name__ == "__main__":
    main()
    
  