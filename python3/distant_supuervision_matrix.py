# distant_supervision_matrix.py
# Rick Kosse s3243508
# date: 11-12-2017
# This program helps with labelling of twitter profiles. 

import pandas as pd

y_actu= []
y_pred= []
tel = 0
with open("labeld_users.test", "r") as infile:
    new_file = [description[:500] for description in infile]


    for line in new_file:

        tel += 1
        print("__________________________________________________________________")
        print(tel, "/", len(new_file))
        print()
        print(line)
        print()
        print("1 : Ajax ")
        print("2 : Feyenoord ")
        print("3 : Fc Groningen ")
        print("4 : Vitesse ")
        print("5 : Go Ahead Eagles ")
        print("6 : onbekend ")
        print()
        invoer = input("Van welke voetbalclub is deze twittergebruiker fan? ")

        if "ajax" in line:
            y_actu.append("ajax")

        elif "feyenoord" in line:
            y_actu.append("feyenoord")
          
        elif "fc groningen"  in line:
            y_actu.append("fc gronignen")

          
        elif "vitesse" in line:
            y_actu.append("vitesse")
         
        elif "go ahead eagles" in line:
            y_actu.append("Gae")
        else:
            y_actu.append("onbekend")

        if invoer == "1":
            y_pred.append("ajax")
        elif invoer == "2":
            y_pred.append("feyenoord")
        elif invoer == "3":
            y_pred.append("fc gronignen")
        elif invoer == "4":
            y_pred.append("vitesse")
        elif invoer == "5":
            y_pred.append("Gae")
        elif invoer == "6":
            y_pred.append("onbekend")

    y_actul = pd.Series(y_pred, name='Actual')
    y_predi = pd.Series(y_actu, name='Predicted')
    telling = 0
    fout = 0
    
    for i, y in zip(y_actu,y_pred):
        if i == y:
            telling += 1
        else:
            fout += 1
    


    df_confusion = pd.crosstab(y_actul, y_predi)
    print("__________________________________________________________________")
    print("________________________resultaten________________________________")
    print(df_confusion)
    print()
    print("aantal goed:", telling)
    print("fout:", fout)