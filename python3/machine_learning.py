# machine_learning.py
# Rick Kosse s3243508
# date: 11-12-2017
# This program takes four files as input, splits it, trains it, and test it. 
# From there it returns the accuracy, F1-score and precission and recall score and most informative features.

import sys
from sklearn.pipeline import FeatureUnion
import nltk
from nltk.corpus import stopwords
from collections import defaultdict 
from sklearn.base import BaseEstimator,TransformerMixin
from collections import Counter
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score, StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score, precision_recall_fscore_support, adjusted_rand_score, classification_report 
import numpy as np
import random
from nltk.tokenize import TweetTokenizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import preprocessing
from sklearn.preprocessing import MultiLabelBinarizer
import pandas as pd




class Featurizer(TransformerMixin):
    """Our own featurizer: extract features from each document for DictVectorizer"""

    PREFIX_WORD_NGRAM="W:"

    def __init__(self,word_ngrams="1",binary=True,lowercase=False,remove_stopwords=True):
        """
        binary: whether to use 1/0 values or counts
        lowercase: convert text to lowercase
        remove_stopwords: True/False
        """
        self.data = [] # will hold data (list of dictionaries, one for every instance)
        self.lowercase=lowercase
        self.binary=binary
        self.remove_stopwords = remove_stopwords
        self.stopwords = stopwords.words('dutch')
        self.word_ngrams=word_ngrams

    
    def fit(self, x, y=None):
        return self
    
    def transform(self, X):
        """
        here we could add more features!
        """
        out= [self._word_ngrams(text,ngram=self.word_ngrams)
                for text in X]
        return out
        
    def _word_ngrams(self,text,ngram="1-2-3"):
        
        d={} #dictionary that holds features for current instance
        if self.lowercase:
            text = text.lower()
            print(text)
        words=text
        if self.remove_stopwords:
            words = [w for w in words if w not in self.stopwords]


        for n in ngram.split("-"):
            for gram in nltk.ngrams(words, int(n)):
                gram = self.PREFIX_WORD_NGRAM + "_".join(gram)
                if self.binary:
                     d[gram] = 1 #binary
                else:
                    d[gram] += 1
        return d
        
    
if __name__ == "__main__":
    import doctest
    doctest.testmod()


def load_sentiment_sentences_and_labels():
    """
    loads the data
    """
    le = preprocessing.LabelEncoder()
    tknzr = TweetTokenizer()
    psv_users = open("/Users/rickkosse/Documents/RUG/scriptie/twitter API/machinelearningdata/psv.all_training.out").readlines()
    twente_users = open("/Users/rickkosse/Documents/RUG/scriptie/twitter API/machinelearningdata/twente.all_training.out").readlines()
    feye_users = open("/Users/rickkosse/Documents/RUG/scriptie/twitter API/machinelearningdata/feyenoord.all_training.out").readlines()
    ajax_users = open("/Users/rickkosse/Documents/RUG/scriptie/twitter API/machinelearningdata/ajax.all_training.out").readlines()

    #tokenize proces
    psv_tokenized_labeled = \
    [(tknzr.tokenize(profile), 'psv')
        for profile in psv_users]
    feyenoord_tokenized_labeled = \
    [(tknzr.tokenize(profile), 'feyenoord')
        for profile in feye_users]    
    twente_tokenized_labeled = \
    [(tknzr.tokenize(profile), 'twente')
        for profile in twente_users]    
    ajax_tokenized_labeled = \
    [(tknzr.tokenize(profile), 'ajax')
        for profile in ajax_users]   

    #seperate the labels and profiles
    ajax_labels = [labels for accounts,labels in ajax_tokenized_labeled]
    psv_labels = [labels for accounts,labels in psv_tokenized_labeled]
    feye_labels = [labels for accounts,labels in feyenoord_tokenized_labeled]
    twente_labels = [labels for accounts,labels in twente_tokenized_labeled]

    ajax_lines = [accounts for accounts,labels in ajax_tokenized_labeled]
    psv_lines = [accounts for accounts,labels in psv_tokenized_labeled]
    feye_lines = [accounts for accounts,labels in feyenoord_tokenized_labeled]
    twente_lines = [accounts for accounts,labels in twente_tokenized_labeled]

    #concatenate all data
    sentences = np.concatenate([ajax_lines, psv_lines, twente_lines, feye_lines], axis=0)
    labels = np.concatenate([ajax_labels, psv_labels, twente_labels,feye_labels ], axis=0)

    #transform labels to numeric data
    transform = le.fit(labels)
    labels = le.transform(labels)

    # make sure to have a label for every data instance
    assert(len(sentences)==len(labels))
    data = list(zip(sentences,labels))
    random.shuffle(data)
    print("split data..", file=sys.stderr)
    split_point = int(0.80*len(data))
    
    #split the data in training and test data
    sentences = [sentence for sentence, label in data]
    labels = [label for sentence, label in data]
    X_train, X_test = sentences[:split_point], sentences[split_point:]
    y_train, y_test = labels[:split_point], labels[split_point:]

    assert(len(X_train)==len(y_train))
    assert(len(X_test)==len(y_test))

    return X_train, y_train, X_test, y_test

# read input data
random.seed(113)
X_train, y_train, X_test, y_test = load_sentiment_sentences_and_labels()
print("vectorize data..", file=sys.stderr)
featurizer = Featurizer(word_ngrams="1")
vectorizer = DictVectorizer()

classifier = LogisticRegression()

# first extract the features (as dictionaries)
X_train_dict = featurizer.fit_transform(X_train)
X_test_dict = featurizer.transform(X_test)

# then convert them to the internal representation (maps each feature to an id)
X_train = vectorizer.fit_transform(X_train_dict)
X_test = vectorizer.transform(X_test_dict)

# crossvalidation
# print("cross-validation..")
# clf = classifier
# scores = cross_val_score(clf, X_train, y_train, cv=10 )
# print(scores)
# print("Accuracy cross-validation: %0.3f (+/- %0.3f)" % (scores.mean(), scores.std() * 2))


#train
print("train model..", file=sys.stderr)
classifier.fit(X_train, y_train)

#predict
print("predict..", file=sys.stderr)
y_predicted = classifier.predict(X_test)

#print scores
print("Accuracy:", accuracy_score(y_test, y_predicted), file=sys.stderr)
print("F1-score:", f1_score(y_test, y_predicted, average='weighted'))
print("Precision, Recall and F-score ",precision_recall_fscore_support(y_test, y_predicted, average='weighted'))
target_names = ['ajax', 'feyenoord', 'PSV' ,'fc twente']
print("Report",classification_report(y_test, y_predicted, target_names=target_names))

#confusion matrix
y_actul = pd.Series(y_predicted, name='predicted')
y_predi = pd.Series(y_test, name='actual')
df_confusion = pd.crosstab(y_actul, y_predi)
print("Matrix",df_confusion)

#print overlap
print("Overlap..")
overlap =adjusted_rand_score(y_actul, y_predi) 
print(overlap)


#print most infomative features
def show_most_informative_features(vectorizer, clf, n=20):
    feature_names = vectorizer.get_feature_names()
    for i in range(0,len(clf.coef_)):
        coefs_with_fns = sorted(zip(clf.coef_[i], feature_names))
        top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])
        print("i",i)
        for (coef_1, fn_1), (coef_2, fn_2) in top:
            print("\t%.4f\t%-15s\t\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2))

#show_most_informative_features(vectorizer, classifier, n=20)