# README #

This README document explains which steps are necessary to get this application up and running.

### What is this repository for? ###

* This is the repository for a bachelor thesis Information science from the University of Groningen.
* The thesis is about automatic classification of football fans using their tweets.

### How do I get set up? ###

* The thesis includes 4 subfolders.
* In the data folder you will find all the users who are found sorted by football club.
* In the folder Python3 you will find different programs to retrieve, analyse and proces the data.
* In the bash folder there is a small Linux shell program to readout the raw tweets files.
* To run the program you need to install python3 on your computer along with NLTK and Scikit.

### Who do I talk to? ###

* The owner of the Thesis
